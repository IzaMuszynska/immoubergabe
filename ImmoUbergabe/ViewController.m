//
//  ViewController.m
//  ImmoUbergabe
//
//  Created by Izabella Muszyńska on 30/11/2018.
//  Copyright © 2018 Quadriga Informatik GmbH. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>

@interface ViewController () <WKNavigationDelegate,WKUIDelegate>
@property(strong,nonatomic) WKWebView *webView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    NSURL *targetURL = [NSURL URLWithString:@"http://awsquadriga.ddns.net"];
    self.webView = [[WKWebView alloc]init];
    self.webView.UIDelegate = self;
    self.webView.navigationDelegate = self;
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    [self.webView loadRequest:request];
    self.webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y + 22,  self.view.frame.size.width, self.view.frame.size.height-22);//
    [self.view addSubview:self.webView];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}
-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    self.webView.frame = CGRectMake(self.view.frame.origin.x,self.view.frame.origin.y + 22,  size.width, size.height-22);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
