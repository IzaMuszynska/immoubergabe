//
//  main.m
//  ImmoUbergabe
//
//  Created by Izabella Muszyńska on 30/11/2018.
//  Copyright © 2018 Quadriga Informatik GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
